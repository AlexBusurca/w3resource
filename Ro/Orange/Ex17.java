package Ro.Orange;


//solutia este cea propusa de w3resource. eu am adaugat comentariile ca sa inteleg ce se intampla in ea
import java.util.Scanner;
public class Ex17 {
    public static void main(String[] args)
    {
        //variabile care tine numerele binare introduse de user
        long binary1, binary2;

        //i variabila folosita pentru a accesa si modifica elemntele array-ului care va tine rezultatul final
        //reminder este o variabila folosita pentru a stoca 1-ul in caz ca rezulta in urma adunarii bitilor
        int i = 0, remainder = 0;

        //array-ul in care va fi salvat rezultatul adunarii
        int[] sum = new int[20];

        //preia input de la user
        Scanner in = new Scanner(System.in);

        //ii cere userului sa introduca cele 2 numere binare ce trebuie adunate
        System.out.print("Input first binary number: ");
        binary1 = in.nextLong();
        System.out.print("Input second binary number: ");
        binary2 = in.nextLong();

        //in acest loop se intampla adunarea atat timp cat cel putin unul din numerele binare este diferit de 0
        while (binary1 != 0 || binary2 != 0)
        {
            //in stanga egalului seteaza i la rezultatul din dreapta si apoi aduna 1
            //in dreapta foloseste operatorul modulo pentru a determina restul impartirii numarului binar la 10
            //tot in dreapta aduna restul lui binary1 cu restul lui binary2 si cu ce a venit din adunarea precedenta.
            //restul impartirii sumei de mai sus la 2 este, de fapt, cifra cea mai din dreapta a rezultatului adunarii celor 2 nr binare
            sum[i++] = (int)((binary1 % 10 + binary2 % 10 + remainder) % 2);

            //aproape identic afla daca trebuie sa pastreze 1 pentru adunarea urmatorilor 2 biti din fiecare numar.
            //singura diferente este ca aceeasi suma de mai sus este impartita la 2 si nu ne mai intereseaza restul
            remainder = (int)((binary1 % 10 + binary2 % 10 + remainder) / 2);

            //imparte ambele numere la 10 astfel incat sa scape de bit-ul cel mai din dreapta
            binary1 = binary1 / 10;
            binary2 = binary2 / 10;
        }

        //daca dupa loop-il de mai sus remainder-ul e 1 il trece si pe acesta in sum fiindca este unul cel mai din stanga numarului binar rezultat in urma adunarii
        if (remainder != 0) {

        //aici, in solutia originala, au mai adunat 1 la i dupa ce au salvat in array 1 din remainder si nu am inteles de ce
            sum[i/*++*/] = remainder;
        }

        //fiindca aici au scazut 1-ul adunat mai sus si nu are sens ce au facut. asa ca am comentat adunarea de mai sus si scaderea de aici
        //--i;

        //printam rezultatul adunarii celor 2 numere binare incepand cu ultimul elemnt introdus in array si continuand cu celelalte pana ajunge la pozitia 0 inclusiv
        System.out.print("Sum of two binary numbers: ");
        while (i >= 0) {
            System.out.print(sum[i--]);
        }
        System.out.print("\n");
    }
}