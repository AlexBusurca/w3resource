package Ro.Orange;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Ex12 {

    public static void main(String[] args) {
        //citesc inputul user-ului cu un obiect de tip scanner
        Scanner noScanner = new Scanner(System.in);

        //pregatesc un array de tip lista astfel incat sa pot tine in el oricate elemente vrea userul sa introduca
        List<Double> numbers = new ArrayList<>();

        //rog userul sa introduca numere si sa tasteze orice altceva daca vrea sa se operasca
        System.out.println("Type one number at a time and press enter. When done, type a letter");

        //adaug elemente la lista atata timp cat user-ul introduce numere
        while (noScanner.hasNextDouble()){
            numbers.add(noScanner.nextDouble());
            }
        //calculez suma elementelor introduse de user si media aritmetica
        double sum = numbers.stream().mapToDouble(Double::doubleValue).sum();
        double average = sum/numbers.size();

        //ii afisez user-ului informatiile despre ce numere a introdus si care este media lor
        System.out.println("The numbers you've entered are " + numbers.toString() + " and their average is " + average);

    }





}
