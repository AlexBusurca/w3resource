package Ro.Orange;

import java.util.Scanner;

public class Ex6 {
    int a;
    int b;

    public int add() {
        return a+b;
    }

    public int extract() {
        return a-b;
    }

    public int multiply() {
        return a*b;
    }

    public int divide(){
        return a/b;
    }

    public int remainder() {
        return a%b;
    }

    public String toString(){
        return  a + " + " + b + " = " + add() + "\n" +
                a + " - " + b + " = " + extract() + "\n" +
                a + " x " + b + " = " + multiply() + "\n" +
                a + " / " + b + " = " + divide() + "\n" +
                a + " mod " + b + " = " + remainder();
    }

    public static void main(String[] args) {
        Ex6 A = new Ex6();
        Scanner input = new Scanner(System.in);
        System.out.println("Please input first number and then press enter.");
        A.a = input.nextInt();
        System.out.println("Please input second number and then press enter.");
        A.b = input.nextInt();
        System.out.println(A.toString());

    }
}
