package Ro.Orange;

import java.util.Scanner;

public class Ex7 {

    public static void main(String[] args) {
        int constant;
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter a number and then press enter.");
        constant = input.nextInt();
        System.out.println("Thank you. Here is the multiplication table up to 10 for number " + constant);
     for (int i = 0; i<11; i++) {
         System.out.println(constant + " x " + i + " = " + constant*i);
     }
    }
}
