package Ro.Orange;

import java.util.Scanner;

public class Ex5 {
    public static void main(String[] args) {
        int fn;
        int sn;

        Scanner input = new Scanner(System.in);

        System.out.println("Input multiplier and press enter.");
        fn = input.nextInt();

        System.out.println("Input multiplicand and press enter.");
        sn = input.nextInt();

        int result = fn*sn;

        System.out.println("Thank you. " + fn + " multiplied by " + sn + " is equal to " + result);
    }

}
