package Ro.Orange;

import java.util.Scanner;

public class Ex15 {

    static private int a;
    static private int b;
    static private String swap;

    static private Scanner in = new Scanner(System.in);
    private static void setInitNumbers(){
        System.out.println("Enter a number");
        a = in.nextInt();
        System.out.println("A = " + a + "\nEnter another number.");
        b = in.nextInt();
        System.out.println("B = " + b);
        System.out.println("Now type swap and press enter");
        swap = in.next();
    }



    private static void swap1(){
        int temp = a;
        a = b;
        b = temp;
    }

    public static void main(String[] args) {

        Ex15.setInitNumbers();

        while(true) {
            if (Ex15.swap.equals("swap")) {
                Ex15.swap1();
                break;
            } else {
                System.out.println("Please type swap and press enter.");
                Ex15.swap = in.next();
            }
        }
        System.out.println("A = " + a + " and B = " + b);
    }

}
