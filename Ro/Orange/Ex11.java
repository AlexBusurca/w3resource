package Ro.Orange;

public class Ex11 {

    public static void main(String[] args) {
        double pi = Math.PI;
        double radius = 7.5D;
        double area = pi*Math.pow(radius,2);
        double perimeter = 2*pi*radius;

        System.out.println("Perimeter is = " + perimeter + "\nArea is = " + area);
    }

}
