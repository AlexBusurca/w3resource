package Ro.Orange;

public class Ex13 {

    public static void main(String[] args) {

        double width = 5.5D;
        double height = 8.5D;
        double area = width*height;
        double perimeter = 2*(width+height);

        System.out.println("Area is " + width + " * " + height + " = " + area +
                           "\nPerimeter is "+ "2 * (" +width + " + " + height +") = " + perimeter);

    }

}
